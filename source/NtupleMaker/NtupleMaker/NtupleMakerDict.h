#ifndef NtupleMaker_MYANALYSIS_DICT_H
#define NtupleMaker_MYANALYSIS_DICT_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <NtupleMaker/xAODHelper.h>
#include <NtupleMaker/FunctionHelper.h>
#include <NtupleMaker/SelectionHelper.h>
#include <NtupleMaker/SelectionWZ.h>
#include <NtupleMaker/SkimToNtupleTriLep.h>
#include <NtupleMaker/SelectionWW.h>
#include <NtupleMaker/SkimToNtupleWWDiLep.h>

#include <NtupleMaker/SelectionFwdReco.h>
#include <NtupleMaker/SkimToNtupleFwdReco.h>

#endif
